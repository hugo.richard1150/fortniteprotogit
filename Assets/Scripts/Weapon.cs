﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public bool makeDamagePlayer;
    public bool makeDamagePlatform;
    public bool makeBump;

    public float fireRate;
    private float nextFire;

    public GameObject bulletPrefab;

    private PlayersBehaviour playersBehaviour;

    private void Start()
    {
        playersBehaviour = GetComponentInParent<PlayersBehaviour>();
    }

    void Update()
    {
        if (playersBehaviour.leftTarget)
        {
            transform.position = new Vector3(playersBehaviour.transform.position.x - 0.5f, playersBehaviour.transform.position.y, 0);
            transform.rotation = Quaternion.Euler(-90, 0, 0);
        }
        else
        {
            transform.position = new Vector3(playersBehaviour.transform.position.x + 0.5f, playersBehaviour.transform.position.y, 0);
            transform.rotation = Quaternion.Euler(-90, 0, 180);
        }

        if (Input.GetButton("FireP"+playersBehaviour.indexPlayer) && Time.time > nextFire && fireRate > 0)
        {
            nextFire = Time.time + fireRate;
            Shoot();
        }
    }

    private void Shoot()
    {
        GameObject go = Instantiate(bulletPrefab, transform.position, transform.rotation);
        go.GetComponent<Bullet>().BulletBehaviour(makeDamagePlayer, makeDamagePlatform, makeBump);
    }
}
