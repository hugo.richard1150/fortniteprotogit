﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float bulletSpeed;

    private bool makeDamagePlayer;
    private bool makeDamagePlatform;
    private bool makeBumpPlayer;

    private Rigidbody rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    public void BulletBehaviour(bool damagePlayer, bool damagePlatform, bool bumpPlayer)
    {
        makeDamagePlayer = damagePlayer;
        makeDamagePlatform = damagePlatform;
        makeBumpPlayer = bumpPlayer;
    }

    public void Update()
    {
        rb.velocity = -transform.right * bulletSpeed * Time.deltaTime;
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Platform" && makeDamagePlatform)
        {
            other.gameObject.GetComponent<PlateformLife>().Damage(5);
            Destroy(gameObject);
        }

        if (other.gameObject.tag == "Player" && makeDamagePlayer)
        {
            other.gameObject.GetComponent<PlayerLife>().TakeDamage(1);
            Destroy(gameObject);
        }

        if (other.gameObject.tag == "Player" && makeBumpPlayer)
        {
            Vector3 dir = (this.transform.position + other.transform.position).normalized;
            other.gameObject.GetComponent<PlayersBehaviour>().Bump(dir);
            Destroy(gameObject);
        }

    }
}
