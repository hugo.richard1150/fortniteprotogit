﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject panelVictory;
    public Text textVictory;

    public Object scene;

    private void Start()
    {
        Time.timeScale = 1f;        
    }

    public void Victory(int playerIndex)
    {
        Time.timeScale = 0;
        textVictory.text = "Joueur " + playerIndex + " a gagné";
        panelVictory.SetActive(true);
    }

    public void DeathPanel(int playerIndex)
    {
        Time.timeScale = 0;
        textVictory.text = "Joueur " + playerIndex + " est mort";
        panelVictory.SetActive(true);
    }

    public void Reset()
    {
        SceneManager.LoadScene(scene.name);        
    }
}
