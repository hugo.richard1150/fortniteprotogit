﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayersBehaviour : MonoBehaviour
{
    public int indexPlayer;

    private CharacterController controller;
    private Vector3 move = Vector3.zero;

    public float speed;
    public float jumpForce;
    public float gravity;

    public GameManager gameManager;
    private PlayerLife playerLife;

    public bool grounded;

    public bool leftTarget;
    public bool rightTarget;

    public GameObject platformWitness;
    public GameObject platform;

    private void Start()
    {
        controller = GetComponent<CharacterController>();
        playerLife = GetComponent<PlayerLife>();
    }

    private void FixedUpdate()
    {
        grounded = controller.isGrounded;

        if (grounded && move.y < 0)
        {
            move.y = 0f;
        }

        move = new Vector3(Input.GetAxis("HorizontalP" + indexPlayer), 0, 0);
        move = transform.TransformDirection(move);

        controller.Move(move * speed * Time.deltaTime);

        if (Input.GetAxis("VerticalP"+indexPlayer) > 0 && controller.isGrounded)
        {
            move.y += Mathf.Lerp(transform.position.y, transform.position.y + jumpForce, Time.deltaTime);
        }

        move.y += gravity * Time.deltaTime;
        controller.Move(move * Time.deltaTime);
    }

    private void Update()
    {
        //Detection de la direction
        if (Input.GetAxis("HorizontalP"+indexPlayer) < 0)
        {
            leftTarget = true;
            rightTarget = false;
        }
        if (Input.GetAxis("HorizontalP" + indexPlayer) > 0)
        {
            rightTarget = true;
            leftTarget = false;
        }

        //Positionnement de la plateforme temoin
        if (leftTarget)
        {
            platformWitness.transform.position = new Vector3(Mathf.Round(transform.position.x - 1), Mathf.Round(transform.position.y), transform.position.z);
            platformWitness.transform.rotation = Quaternion.Euler(-45, 90, 90);
        }
        if (rightTarget)
        {
            platformWitness.transform.position = new Vector3(Mathf.Round(transform.position.x + 1), Mathf.Round(transform.position.y), transform.position.z);
            platformWitness.transform.rotation = Quaternion.Euler(45, 90, 90);
        }

        //Positionnement de la plateforme en dure
        if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
        {
            GameObject go = Instantiate(platform, platformWitness.transform.position, platformWitness.transform.rotation);
            go.GetComponent<BoxCollider>().isTrigger = false;
        }

        //Check de la vie
        if (playerLife.currentHealth <= 0)
        {
            gameManager.DeathPanel(indexPlayer);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //Quand un joueur touche le drapeau de fin
        if (other.gameObject.tag == "Finish")
        {
            gameManager.Victory(indexPlayer);
        }
    }

    public void Bump(Vector3 dirBullet)
    {
        controller.Move(dirBullet * 10 * Time.deltaTime);
    }
}
